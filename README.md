# Advance Guard and Tannenberg 4

These are my version of [Pelle
Nilsson's](https://boardgamegeek.com/user/pelni) games **Advance
Guard** and **Tannenberg 4**.  I have made them mainly to give
examples of how to use the LaTeX package
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex).  Please see
that page for more information too. 

Other examples of games that uses the `wargame` package are shown on
the [LaTeX Wargames
page](https://wargames_tex.gitlab.io/wargame_www/). 

## Make the documents 

Please see [this
page](https://wargames_tex.gitlab.io/wargame_www/build.html) for
general instructions, including how to install the needed tools on
various platforms. 

To make PDFs for the two documents, you need the LaTeX 
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex)
package. Follow the instructions given on that page.

To make `AdvanceGuard.pdf`, do 

    > make 
	
To make `Tannenberg4.pdf`, do 

	> make NAME=Tannenberg4 
	
If you do not have [`make`](https://gnu.org/software/make), then open
either [`AdvanceGuard.tex`](AdvanceGuard.tex) or
[`Tannenberg4.tex`](Tannenberg4.tex) in your favourite LaTeX editor
and run pdfLaTeX on it twice. 

## VASSAL module 

An example of generating a VASSAL module from the sources of the games
is provided.  This only applies to the _Tannenberg4_ game. 

For this game, the definitions of counters, board, tables, and such,
has been put into a separate package
[`tannenberg4.sty`](tannenberg4.sty) which is used both for generating
the PDF materials, and when we make a VASSAL module.

Next, we have a simple LaTeX document
[`Tannenberg4Export.tex`](Tannenberg4Export.tex) which uses the
package to make single page images of the counters, board, tables, and
such.  Another side effect of running LaTeX on this, is that it will
generate the file `Tannenberg4Export.json` which catalogues the
images using some simple tags.  These tags identifies images as
counters, boards, charts, OOBs, splash image, pool images, or
something else.  Furthermore, each image can be sub-classified,
typically by the faction. 

Once we have made the rules `Tannenberg4.pdf` and the two export files
`Tannenberg4Export.pdf` and `Tannenberg4Export.json`, we can convert
this into a VASSAL module using a Python script in the `wargame`
package.  That script allows us to _monkey-patch_ the generated module
via Python.  So we also have the script
[`Tannenberg4.py`](Tannenberg4.py) which does just that.  The script
basically fixes up the grid and adds in starting position of
counters. 

We then run the script provided with `wargame` like 

	export.py Tannenberg4Export.pdf Tannenberg4Export.csv \
		-t Tannenbert4 -v 0.1 -r Tannenberg4.pdf \
		-p Tannenberg4.py -o Tannenberg4.vmod 
		
The `-r` option tells the script to include the rules file in the
final module, so that the player has everything in one place.  The
options `-t` and `-v` sets the title and version of the module,
respectively. 

Even though the module generated above is fairly complete, one could
still open the module in the VASSAL editor and flesh it out even more,
before publishing it.

The file [`Tannenberg4.vlog`](Tannenberg4.vlog) is a play-through
using VASSAL. 
 
## The original games 

Both games were designed and made by [Pelle
Nilsson's](https://boardgamegeek.com/user/pelni) and have both been
licensed under [Create Commons Attribution-ShareAlike license, version
	3.0](http://creativecommons.org/licenses/by-sa/3.0/). 

The [original **Tannenberg
4**](https://boardgamegeek.com/thread/383408/article/3170712#3170712)
was part of a Board Game Geek (BGG)
[contest](https://boardgamegeek.com/thread/383408/contest-4-cards-or-tiles-only-win-15gg)
to create a game using only 4 cards or tiles. 

The [original **Advance
Guard**](https://boardgamegeek.com/thread/806508/2012-pnp-solitaire-contest-advance-guard-1914-cont/page/1)
was part of a BGG solitaire wargame contest. 

## Differences from originals 

**Tannenberg 4** was originally made as one image (using
[Inkscape(?)](https://inkscape.org)) and a forum post on BGG,
containing the rules. 

The rules for **Advance Guard** was made using
[LaTeX](https://latex.org), and the graphics with
[Inkscape](https://inkscape.org). 

These version uses LaTeX _only_, with the help of my
[`wargame`](https://gitlab.com/wargames_tex/wargame_tex) package.
NATO App6 symbology has been used as far as possible, and the rules
have been slightly restructured. Examples and a game replay were added
to **Tannenberg 4**. 

## The sources 

The source ([`Tannenberg4.tex`](Tannenberg4.tex)) for **Tannenberg 4**
is particularly simple (as is the game).  The example shows how to
define counters (units), a board, create examples using the board and
counters, and how to make a "replay" of a game. 

The source ([`AdvanceGuard.tex`](AdvanceGuard.tex)) is a bit more
elaborate.  This shows how to make "base" counter types, a more
elaborate board, and tables and charts.

The sources are heavily commented (`%`) to help the reader understand
what is going on.  A bit of LaTeX proficiency is required though. 


## License 

This work is licensed under [CC BY-SA
4.0](http://creativecommons.org/licenses/by-sa/4.0/) 

![CC](https://mirrors.creativecommons.org/presskit/icons/cc.svg)
![BY](https://mirrors.creativecommons.org/presskit/icons/by.svg)
![SA](https://mirrors.creativecommons.org/presskit/icons/sa.svg)

