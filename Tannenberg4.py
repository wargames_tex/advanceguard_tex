from wgexport import *

# --------------------------------------------------------------------
# some utilities 
def print_tree(doc,ind=''):
    print(f'{ind}{doc}')
    for c in doc.childNodes:
        print_tree(c,ind+' ')

def print_attr(doc):
    from pprint import pprint

    pprint(doc.attributes.items())
    
# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):
    from pprint import pprint

    game = build.getGame()
    
    # ----------------------------------------------------------------
    # Get all boards 
    maps = game.getMaps()
    mainMap = maps['Board']

    # ----------------------------------------------------------------
    # Get all boards 
    boards = mainMap.getBoards()

    # ----------------------------------------------------------------
    # The main board.  We will work on this first 
    mainBoard = boards['Board']

    # ----------------------------------------------------------------
    # Add zone for turn track 
    #
    # First, get the zoned area 
    zoned = mainBoard.getZonedGrids()[0]
    # We must add our Turn zone _before_ the hex zone, so first we get
    # that zone
    hex_zone = zoned.getZones()['full']
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing 
    hex_grid            = hex_zone.getHexGrids()[0]
    hex_grid['visible'] = False
    # Numbering - We turn off drawin and set the starting row number 
    hex_grid_number             = hex_grid.getNumbering()[0]
    hex_grid_number['vOff']     = -4
    hex_grid_number['visible']  = False
    hex_grid_number['vDescend'] = True
    
    print(hex_grid_number)

    # Now we remove that child from zoned
    zoned.remove(hex_zone)
    # Then add a zone (we can use the debug window to read off coordinates)
    turn_zone = zoned.addZone(name = 'Turn',
                              path='79,707;911,707;911,788;79,788',
                              useHighlight=True)
    turn_grid = turn_zone.addSquareGrid(color=rgb(255,0,0),
                                        x0=42,
                                        y0=0,
                                        dx=82.8,
                                        dy=82.8,
                                        visible=False)
    turn_grid.addNumbering(sep='T',
                           hOff=0,
                           vOff=-9,
                           visible=False)
    # We add back in the full zone
    if not zoned.append(hex_zone):
        print('Failed to add back hex zone')

    # ----------------------------------------------------------------
    # Add starting units
    #
    # Get all the counter
    counters = game.getPieces(asdict=True)

    # Add at start stacks
    for u,h in [['german 8', 'D5'],
                ['russian 1', 'I6'],
                ['russian 2', 'D2']]:
        mainMap.addAtStart(name=h,
                           owningBoard='Board',
                           location = h,
                           useGridLocation = True).addPieces(counters[u])
    mainMap.addAtStart(name='Turn 1',                       
                       owningBoard='Board',
                       location = '01T0',
                       x = 123,
                       y = 748,
                       useGridLocation = True)\
        .addPieces(counters['game turn chit'])

    if verbose:
        print('Done patching')
